Alpine Forest Environment v1.0
------------------------------

This pack contains models, textures and prefabs to create an Alpine Forest Environment.
Created in Unity 5.2.3f1.

Instructions to load the demo scene:
- Start a new project and choose 3D as the type.
- Import the Alpine_Forest_Environment.unitypackage
- open the folder and run: Assets/GameTechnix/Alpine Forest Environment/Scenes/Alpine Forest Environment/Alpine Forest Environment.unity
- Standard Image effects are not included in this package.
- Image effects used in screenshots:
	- SSAO
	- Sun Shafts
	- bloom
	- Vignette and Chromatic Aberration
	- Colour correction curves

The Contents of this package contains:

5 prefab models with textures and Normal-maps
	- Alpine Log Cabin
	- Alpine Lookout Tower
	- Alpine Stone Cabin
	- Alpine Wood House
	- Wooden Jetty

10 Terrain ground textures with Normal-maps and Heightmaps for use in the terrain editor or RTP (relief Terrain Pack)
	- 2 x cobble stone paths
	- 2 x cliff textures for different UV co-ordinates
	- 1 x pebble stones
	- 1 x pine needles
	- 1 x Grass
	- 1 x rocky-grass
	- 1 x rocky-ground
	- 1 x Snow 

10 Grass Photo detail textures with alpha for use in the terrain editor
	- Alpine Flowers 1
	- Alpine Flowers 2
	- Alpine Grass 1
	- Alpine Grass 2
	- Alpine Grass 3
	- Alpine Grass 4
	- Pine forest grass 1
	- Pine forest grass 2
	- Pine forest skinny grass
	- Pine forest Tuft Grass

13 Tree model prefabs for use in the terrain editor
	- 1 x Young Pine Tree
	- 1 x Fern
	- 6 x Green Pine/Fur Trees
	- 5 x Snow Pine/Fur Trees

1 Alpine 6 sided Cubemap Sky for use in the environment skybox
	- 1 Sunset Alpine Sky 6 sided Cubemap

1 Alpine Forest Environment Demonstration scene
	- 1 x Alpine Unity scene for demonstration of use of Assets

Instructions for RTP (Relief Terrain Pack) setup
------------------------------------------------
- Import RTP Asset pack
- setup only 8 terrain textures
- Watch the RTP tutorial video for instructions of use
- apply the ReliefTerrain script to terrain
- Set _RTP_LODManager to 8 layers and POM max LOD
- Setup the 8 terrain textures in the RTP terrain setup
- include the _H textures in the heightmap section
- in Terrain -> Relief Terrain (Script)
	- Click on settings panel
        - in Main panel, set tile size between 7 and 9
	- in Parallax panel, adjust extrude height to suit
	

For help or questions about these Assets, please contact me at gametechnix@gmail.com
http://virtex3d.com.au/gametechnix/
