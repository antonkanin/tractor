﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumOfLevel : MonoBehaviour
{
    public int Index;

    private int m_currentJob = 0;

    public IntValue m_activeJobsCounter;

    private void Start()
    {
        ResetJobsCounter();
        LoadJobs(m_currentJob);
    }

    private void LoadJobs(int jobIndex)
    {
        var jobs = GetComponent<QuestListTractor>().ListOfQuests.Quests[Index].WorkOrderOnField;

        for (int i = 0; i < transform.childCount; i++)
        {
            var cube = transform.GetChild(i);
            var plantScript = cube.GetComponent<PlantScript>();
            if (plantScript != null)
            {
                plantScript.CurrentJobType = jobs[jobIndex];
            }
        }
    }

    public void LoadNextJob()
    {
        m_currentJob++;
        ResetJobsCounter();
        LoadJobs(m_currentJob);
    }
    private void ResetJobsCounter()
    {
        m_activeJobsCounter.Value = transform.childCount;
    }
}
