﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class PlantScript : MonoBehaviour
{
    private KindOfWork m_kind;
    [SerializeField] private GameObject m_field;
    private TrailerType m_trailer;
    public List<KindOfWork> WorkOrderPlant;
    public IntValue m_activeJobsCounter;
    private int m_numOfMissions;
    private int m_currentMission;
    private int m_numOfLevel;
    private KindOfWork m_currentJobType;

    public KindOfWork CurrentJobType
    {
        get
        {
            return m_currentJobType;
        }

        set
        {
            m_currentJobType = value;
            for (int i = 0; i < transform.childCount; i++)
            {
                var cube = transform.GetChild(i).gameObject;
                var jobtype = cube.GetComponent<JobType>();
                if (jobtype != null && jobtype.m_jobType == value)
                {
                    cube.SetActive(true);
                }
                else
                {
                    cube.SetActive(false);
                }
            }
        }
    }

    private void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (!other.CompareTag("Trailer"))
        {
            return;
        }

        m_trailer = other.GetComponent<TrailerType>();
        if (m_trailer == null)
        {
            return;
        }

        if (m_trailer.TrailerEnum == CurrentJobType)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
                m_activeJobsCounter.Value--;
            }
        }

        if (m_activeJobsCounter.Value == 0)
        {
            m_field.GetComponent<NumOfLevel>().LoadNextJob();
        }
    }

    private static void DeactivateAllCubes()
    {
        foreach (var cube in GameObject.FindGameObjectsWithTag("Cube"))
        {
            cube.SetActive(false);
        }
    }

    private void WaterAmount()
    {
        var m_water = m_trailer.AmountOfWater;
        if (m_water > 0)
        {
            m_trailer.AmountOfWater -= 0.2f;
            if (m_water <= 10)
            {
                Debug.Log("Water Is Almost Over");
            }
        }
        else
        {
            m_trailer.IsReadyForUse = false;
            Debug.Log("Need Water To Work!");
        }
    }


    private void ActivateGreenCube()
    {
        transform.GetChild(m_currentMission).gameObject.SetActive(true);
        //Debug.Log(transform.childCount);
    }


}