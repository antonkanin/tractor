﻿using UnityEngine;
using TMPro;

public class TrailerType : MonoBehaviour
{
    public KindOfWork TrailerEnum;
    public bool IsReadyForUse = false;
    public float  AmountOfWater = 0;
    [SerializeField] private TextMeshPro m_amount;

    private void Update()
    {
        m_amount.text = (int)AmountOfWater + "%";
    }
}
