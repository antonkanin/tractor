﻿    public enum KindOfWork
    {
        Plow = 0,
        Sow = 1,
        ToWater = 2,
        Fertilize = 3,
        Collect = 4
    }
