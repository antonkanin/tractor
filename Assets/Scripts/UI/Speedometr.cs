﻿using UnityEngine;
using TMPro;

public class Speedometr : MonoBehaviour {

    [SerializeField] private Rigidbody tractorRb;

    private TextMeshProUGUI speedoMetr;

    private void Start()
    {
        speedoMetr = GetComponent<TextMeshProUGUI>();
    }

    void Update () {
        var tractorSpeed = Mathf.Round( tractorRb.velocity.magnitude * 3.6f);
        //Debug.Log(tractorSpeed);
        speedoMetr.text = (tractorSpeed.ToString()+"kmh");
    }
}
