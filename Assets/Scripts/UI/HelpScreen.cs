﻿using UnityEngine;

public class HelpScreen : MonoBehaviour {

    private bool toggle = false;
    
    public void changeHelpOn()
    {
        if (!toggle)
        {
            gameObject.SetActive(true);
            toggle = true;
        }
        else
        {
            gameObject.SetActive(false);
            toggle = false;
        }
    }
}
