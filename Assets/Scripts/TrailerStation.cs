﻿using UnityEngine;

public class TrailerStation : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        var trailer = other.GetComponent<TrailerType>();
        if (!other.CompareTag("Trailer")) return;
        trailer.IsReadyForUse = true;
        trailer.AmountOfWater = 100;
    }
}
