﻿using UnityEngine;

public class Connector : MonoBehaviour {
    [SerializeField] private Rigidbody m_tractor;
    [SerializeField] private BoxCollider m_tractorcol;
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Tractor") || gameObject.GetComponent<ConfigurableJoint>() != null) return;
        Debug.Log("Trailer hooked!");
        var joint = gameObject.AddComponent<ConfigurableJoint>();
        GetComponent<Rigidbody>().isKinematic = false;
        //joint setup
        joint.anchor = new Vector3(0,-0.3f,2.5f); 
        joint.xMotion = ConfigurableJointMotion.Locked;
        joint.yMotion = ConfigurableJointMotion.Locked;
        joint.zMotion = ConfigurableJointMotion.Locked;
        joint.enablePreprocessing = false;
        joint.enableCollision = true;
        joint.connectedBody = m_tractor;
        joint.massScale = 10;
        //m_tractorcol.enabled = false;

    }
}
