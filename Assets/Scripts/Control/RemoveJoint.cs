﻿using UnityEngine;

public class RemoveJoint : MonoBehaviour {
    [SerializeField] private BoxCollider jointCollider;
    [SerializeField] private float boxColOnTime;

    private void Update ()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Destroy( GetComponent<ConfigurableJoint>());
            jointCollider.enabled = false;
        }

        if (jointCollider.enabled == false)
        {
            boxColOnTime -= Time.deltaTime;
            if (boxColOnTime < 0)
            {
                jointCollider.enabled = true;
                boxColOnTime = 2f;
            }
        }
    }
}
