﻿
using UnityEngine;

public class CenterOfMass : MonoBehaviour {
    [SerializeField] private Transform m_centerOfmass;
	// Use this for initialization
    private void Start () {
        GetComponent<Rigidbody>().centerOfMass = m_centerOfmass.localPosition;
	}
	
	
}
