﻿using System;
using UnityEngine;

public class TractorControl : MonoBehaviour
{
    [Header("WheelColliders:")]
    [SerializeField] private WheelCollider[] frontCols;
    [SerializeField] private WheelCollider[] backCols;

    [Header("Meshes:")]
    [SerializeField] private Transform[] meshFront;
    [SerializeField] private Transform[] meshBack;
    [SerializeField] private Transform centerOfMass;
    [SerializeField] private GameObject steeringWheel;

    [Header("Settings:")]
    [SerializeField] private float enginePower = 300f;
    [SerializeField] private float sideSpeed = 35f;
    [SerializeField] private float breakSpeed = 5f;
    [SerializeField] private float maxSpeed = 60f;

    private float vAxis;
    private float hAxis;
    private float bAxis;
    private Rigidbody tractorRb;

	void Start ()
    {
        tractorRb = GetComponent<Rigidbody>();
        tractorRb.centerOfMass = centerOfMass.localPosition;
        
    }
   
    void FixedUpdate ()
    {
        vAxis = Input.GetAxis("Vertical");
        hAxis = Input.GetAxis("Horizontal");
        bAxis = Input.GetAxis("Jump");

        CheckBrakes(breakSpeed*bAxis);

        MoveForward(vAxis * enginePower);
        MoveSide(hAxis * sideSpeed);
        MaxSpeed();

        SteeringWheelRotation();
        WheelRotate();
        Debug.Log(tractorRb.velocity.magnitude);
    }
    
    private void SteeringWheelRotation()
    {
        steeringWheel.transform.localEulerAngles = new Vector3(
            steeringWheel.transform.localEulerAngles.x, 
            steeringWheel.transform.localEulerAngles.y, 
            -hAxis * 360);
    }

    private void MoveForward(float torque)
    {
        frontCols[0].motorTorque = torque;
        frontCols[1].motorTorque = torque;
        backCols[0].motorTorque = torque;
        backCols[1].motorTorque = torque;
    }

    private void MoveSide(float angle)
    {
        frontCols[0].steerAngle = angle;
        frontCols[1].steerAngle = angle;
    }

    private void CheckBrakes(float breakPower)
    {
        frontCols[0].brakeTorque = breakPower;
        frontCols[1].brakeTorque = breakPower;
        backCols[0].brakeTorque = breakPower;
        backCols[1].brakeTorque = breakPower;
    }

    private void WheelMeshRotation(WheelCollider col, Transform dataWheel)
    {
        Vector3 position;
        Quaternion rotation;
        col.GetWorldPose(out position, out rotation);
        dataWheel.position = position;
        dataWheel.rotation = rotation;
    }

    private void WheelRotate()
    {
        WheelMeshRotation(frontCols[0], meshFront[0]);
        WheelMeshRotation(frontCols[1], meshFront[1]);
        WheelMeshRotation(backCols[0], meshBack[0]);
        WheelMeshRotation(backCols[1], meshBack[1]);
    }
    
    private void MaxSpeed()
    {
        if (tractorRb.velocity.magnitude > maxSpeed/3.6f)
        {
            tractorRb.velocity = Vector3.ClampMagnitude(tractorRb.velocity,maxSpeed/3.6f);
        }
    }

}
