﻿using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class WorkPlan
{
    public List<KindOfWork> WorkOrderOnField;
}

[Serializable]
public class QuestList
{
    public List<WorkPlan> Quests;
}

public class QuestListTractor : MonoBehaviour
{
    public QuestList ListOfQuests;
}



